use std::{env, process};

mod initialize;
mod show_help;

fn command_line_interface() {
    let args: Vec<String> = env::args().collect();
    const VERSION: &'static str = env!("CARGO_PKG_VERSION");

    for i in 1..args.len() {
        match args[i].as_str() {
            "--help"    => show_help::main(),
            "--version" => {
                println!("{}", VERSION);
                process::exit(0);
            },
            _           => println!("Unknown argument: {}", &args[i]),
        }
    }

    initialize::initialize();
}

fn main() {
    command_line_interface();

    process::exit(0);
}
