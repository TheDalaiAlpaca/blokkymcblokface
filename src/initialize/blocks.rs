pub struct Blok {
    pub name:    String,
    pub id:      u8,
//    pub texture: &'static str,
    pub texture: Vec<&'static str>,
}


pub fn texture_to_vec(raw_texture: &'static str) -> Vec<&'static str> {
    // split string delimited by \n
    let mut texture: Vec<&'static str> = raw_texture.split("\n").collect();
    texture.truncate(4);
    texture
}


pub fn contruct_bloks() -> Vec<Blok> {
    let blok_dirt = Blok {
        name:    String::from("dirt"),
        id:      0,
        texture: texture_to_vec(include_str!("resources/blocks/dirt.block")),
    };

    let blok_gravel = Blok {
        name:    String::from("gravel"),
        id:      1,
        texture: texture_to_vec(include_str!("resources/blocks/gravel.block")),
    };

    let blok_sky = Blok {
        name:    String::from("sky"),
        id:      2,
        texture: texture_to_vec(include_str!("resources/blocks/sky.block")),
    };

    let blok_lava = Blok {
        name:    String::from("lava"),
        id:      3,
        texture: texture_to_vec(include_str!("resources/blocks/lava.block")),
    };

    let blok_sand = Blok {
        name:    String::from("sand"),
        id:      4,
        texture: texture_to_vec(include_str!("resources/blocks/sand.block")),
    };

    let blok_cobblestone = Blok {
        name:    String::from("cobblestone"),
        id:      5,
        texture: texture_to_vec(include_str!("resources/blocks/cobblestone.block")),
    };

    let blok_water = Blok {
        name:    String::from("water"),
        id:      6,
        texture: texture_to_vec(include_str!("resources/blocks/water.block")),
    };

    let bloks: Vec<Blok> = vec![
        blok_dirt,
        blok_gravel,
        blok_sky,
        blok_lava,
        blok_sand,
        blok_cobblestone,
        blok_water,
    ];

    bloks
}

// Blokfield type of several conjoined bloks

// fn to convert blok texture lines to a vec

// fn get_blok() to get what blok is at particular coords of a Blokfield

// fn set_blok() to return a blokfield with a particular blok changed
