use std::process;

pub fn main() {
print!(
"USAGE
    blokkymcblokface OPTION

DESCRIPTION
    This program doesn't do very much.

OPTIONS
    --help     show this help text
    --version  display version information
"
);

process::exit(0);
}
